<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 9);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",4) ?>
<style>
.bg-interna{
  background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top center no-repeat;
}
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php
  $voltar_para = ''; // link de volta, exemplo produtos, dicas, servicos etc
  require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->



  <!-- ======================================================================= -->
  <!--  titulo geral -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row ">
      <div class="col-6  text-center top220">
        <h4><span>DICAS</span></h4>
      </div>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!--  titulo -->
  <!-- ======================================================================= -->



  <!-- ======================================================================= -->
  <!-- CATEGORIAS HOME -->
  <!-- ======================================================================= -->
  <div class="container-fluid bg_escuro">
    <div class="row top80">
      <div class="container">
        <div class="row lista_categorias">

          <div class="col-4">
            <div class="media">
              <img class="d-flex mr-3" src="<?php echo Util::caminho_projeto() ;?>/imgs/icon_produtos_destaque.png" alt="">
              <div class="media-body">
                <h2 class="mt-0"><span>CONFIRA ALGUMAS DICAS</span></h2>
              </div>
            </div>
          </div>

          <div class="col-8 padding0 top10">
            <div id="slider1" class="flexslider">
              <ul class="slides">

                <li class="active">
                  <a class="btn" href="<?php echo Util::caminho_projeto() ?>/produtos" title="VER TODOS" >
                    VER TODOS
                  </a>
                </li>

                <?php
                $i=0;
                $result = $obj_site->select("tb_categorias_produtos");
                if (mysql_num_rows($result) > 0) {

                  while ($row = mysql_fetch_array($result)) {
                    ?>
                    <li >
                      <a  class="btn" href="<?php echo Util::caminho_projeto() ?>/produtos/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>" >
                        <?php Util::imprime($row[titulo],18); ?>
                      </a>
                    </li>
                    <?php
                  }

                }
                ?>

              </ul>
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- CATEGORIAS HOME -->
  <!-- ======================================================================= -->


  <div class="container-fluid bg_cinza">
    <div class="row">
      <div class="container">
        <div class="row ">
          <div class="col-6">
            <ol class="breadcrumb endereco_sites top15">
              <li class="breadcrumb-item"><a href="<?php echo $link_topo ; ?>"><i class="fa fa-arrow-circle-o-left fa-2x right5"></i></a></li>
              <li class="breadcrumb-item top5"><a href="<?php echo Util::caminho_projeto() ?>/">HOME</a></li>
              <li class="breadcrumb-item top5 active">DICAS</li>
            </ol>
          </div>
          <!--  ==============================================================  -->
          <!--   PESQUISAR -->
          <!--  ==============================================================  -->
          <div class="col-3 procura_empresa">
            <div class="pg5">
              <form action="<?php echo Util::caminho_projeto() ?>/produtos/" method="post">
                <div class="input-group">
                  <input type="text" class="form-control" name="busca_produtos" placeholder="PESQUISAR PRODUTOS" aria-label="PESQUISAR PRODUTOS">
                  <span class="input-group-btn">
                    <button class="btn border-0" type="submit"><span class="fa fa-search"></span></button>
                  </span>
                </div>
              </form>
            </div>
          </div>
          <!--  ==============================================================  -->
          <!--   PESQUISAR -->
          <!--  ==============================================================  -->

          <div class="col-3 top10">
            <a href="<?php echo Util::caminho_projeto() ?>/produtos" class="btn btn-lg btn_orcamento_empresa" title="SOLICITAR UM ORÇAMENTO">
              SOLICITAR ORÇAMENTO
            </a>
          </div>

        </div>
      </div>
    </div>
  </div>







    <!--  ==============================================================  -->
    <!--   DICAS -->
    <!--  ==============================================================  -->
    <div class="container bottom135">
      <div class="row dicas_home">


        <?php
        $result = $obj_site->select("tb_dicas");
        if(mysql_num_rows($result) > 0){
          while($row = mysql_fetch_array($result)){
            $i=0;
            ?>

            <div class="col-4 bottom50 top40">
              <div class="card bottom50">
                <a href="<?php echo Util::caminho_projeto() ?>/dica/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
                  <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 370, 268, array("class"=>"card-img-top", "alt"=>"$row[titulo]")) ?>
                </a>
                <div class="card-body ">
                  <div class="top90 dica_line_titulo">
                    <h3 class="text-uppercase"><span><?php Util::imprime($row[titulo]); ?></span></h3>
                  </div>
                  <div class="dica_line top10">
                    <p class="card-text"><?php Util::imprime($row[descricao]); ?></p>
                  </div>

                  <div class="top20">
                    <a class="btn btn_detalhes_dicas" href="<?php echo Util::caminho_projeto() ?>/dica/<?php Util::imprime($row[url_amigavel]); ?>"  title="<?php Util::imprime($row[titulo]); ?>">
                      MAIS DETALHES
                    </a>
                  </div>

                </div>
              </div>
            </div>

            <?php
            if ($i==2) {
              echo "<div class='clearfix'>  </div>";
            }else {
              $i++;
            }
          }
        }
        ?>

      </div>
    </div>
    <!--  ==============================================================  -->
    <!--   DICAS -->
    <!--  ==============================================================  -->



    <!--  ==============================================================  -->
    <!--   PRODUTOS -->
    <!--  ==============================================================  -->
    <div class="container-fluid ">
      <div class="row relativo">
        <div class="barra_paginas w-100"></div>

        <div class="container pb50 top10">
          <div class="row produtos">

            <div class="col-12 ">
              <div class="media">
                <img class="d-flex mr-3" src="<?php echo Util::caminho_projeto() ;?>/imgs/icon_produtos_destaque.png" alt="">
                <div class="media-body">
                  <h2 class="mt-0"><span>CONHEÇA NOSSOS PRODUTOS</span></h2>
                </div>
              </div>
            </div>

            <?php require_once('./includes/lista_produtos_paginas.php') ?>


          </div>
        </div>

      </div>
    </div>
    <!--  ==============================================================  -->
    <!--   PRODUTOS -->
    <!--  ==============================================================  -->

    <!-- ======================================================================= -->
    <!-- rodape    -->
    <!-- ======================================================================= -->
    <?php require_once('./includes/rodape.php') ?>
    <!-- ======================================================================= -->
    <!-- rodape    -->
    <!-- ======================================================================= -->



  </body>

  </html>


  <?php require_once('./includes/js_css.php') ?>

  <script type="text/javascript">
  $(window).load(function() {
    $('.flexslider').flexslider({
      animation: "slide",
      animationLoop: true,
      controlNav: false,/*tira bolinhas*/
      itemWidth: 270,
      itemMargin: 0,
      inItems: 1,
      maxItems: 20
    });
  });
  </script>
