<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 7);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",7) ?>
<style>
.bg-interna{
  background: #EBECE7 url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top center no-repeat;
  z-index: 999 !important;
}
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->



  <!-- ======================================================================= -->
  <!--  titulo geral -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row ">
      <div class="col-6  text-center top220">
        <h4><span>CONTATOS</span></h4>
      </div>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!--  titulo -->
  <!-- ======================================================================= -->



  <!-- ======================================================================= -->
  <!-- CATEGORIAS HOME -->
  <!-- ======================================================================= -->
  <div class="container-fluid bg_escuro">
    <div class="row top80">
      <div class="container">
        <div class="row lista_categorias">

          <div class="col-4">
            <div class="media">
              <img class="d-flex mr-3" src="<?php echo Util::caminho_projeto() ;?>/imgs/icon_produtos_destaque.png" alt="">
              <div class="media-body">
                <h2 class="mt-0"><span>FALE CONOSCO</span></h2>
              </div>
            </div>
          </div>

          <div class="col-8 padding0 top10">
            <div id="slider1" class="flexslider">
              <ul class="slides">

                <li class="active">
                  <a class="btn" href="<?php echo Util::caminho_projeto() ?>/produtos" title="VER TODOS" >
                    VER TODOS
                  </a>
                </li>

                <?php
                $i=0;
                $result = $obj_site->select("tb_categorias_produtos");
                if (mysql_num_rows($result) > 0) {

                  while ($row = mysql_fetch_array($result)) {
                    ?>
                    <li >
                      <a  class="btn" href="<?php echo Util::caminho_projeto() ?>/produtos/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>" >
                        <?php Util::imprime($row[titulo],18); ?>
                      </a>
                    </li>
                    <?php
                  }

                }
                ?>

              </ul>
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- CATEGORIAS HOME -->
  <!-- ======================================================================= -->


  <div class="container-fluid bg_cinza">
    <div class="row">
      <div class="container">
        <div class="row ">
          <div class="col-4">
            <ol class="breadcrumb endereco_sites top15">
              <li class="breadcrumb-item"><a href="<?php echo $link_topo ; ?>"><i class="fa fa-arrow-circle-o-left fa-2x right5"></i></a></li>
              <li class="breadcrumb-item top5"><a href="<?php echo Util::caminho_projeto() ?>/">HOME</a></li>
              <li class="breadcrumb-item top5 active">FALE CONOSCO</li>
            </ol>
          </div>

          <!-- ======================================================================= -->
          <!-- menu -->
          <!-- ======================================================================= -->
          <div class="col-8 top25 menu_contatos">
            <ul class="nav nav-pills nav-fill text-center">
              <li class="nav-item">
                <a class="nav-link active" href="#">
                  FALE CONOSCO
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#"  onclick="$('html,body').animate({scrollTop: $('.mapa').offset().top}, 1000);">
                  ONDE ESTAMOS
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="<?php echo Util::caminho_projeto() ?>/trabalhe-conosco">
                  TRABALHE CONOSCO
                </a>
              </li>
            </ul>
          </div>
          <!-- ======================================================================= -->
          <!-- menu -->
          <!-- ======================================================================= -->


        </div>
      </div>
    </div>
  </div>




  <div class="container-fluid bg_fale ">
    <div class="row">

      <div class="container top40 relativo">
        <form class="FormContatos" role="form" method="post" enctype="multipart/form-data">
          <div class="row ">

            <div class="col-4">

              <div class="w-75">
                <h5>ENVIE SUAS INFORMAÇÕES OU DÚVIDAS</h5>
              </div>

              <div class=" top15 w-50">
                <h6>SE PREFERIR, LIGUE AGORA:</h6>
              </div>


              <!-- ======================================================================= -->
              <!-- telefones  -->
              <!-- ======================================================================= -->
              <div class="telefone_fale top20">
                <div class="media">
                  <i class="d-flex fa fa-phone fa-2x" aria-hidden="true"></i>
                  <div class="media-body align-self-center">
                    <h2 class="mt-0 ml-3"><?php Util::imprime($config[ddd1]); ?> <?php Util::imprime($config[telefone1]); ?></h2>
                  </div>
                </div>
              </div>


              <?php if (!empty($config[telefone2])): ?>
                <div class="telefone_fale">
                  <div class="top10 media">
                    <i class="d-flex fa fa-whatsapp fa-2x" aria-hidden="true"></i>
                    <div class="media-body align-self-center">
                      <h2 class="mt-0 ml-3"><?php Util::imprime($config[ddd2]); ?> <?php Util::imprime($config[telefone2]); ?></h2>
                    </div>
                  </div>
                </div>
              <?php endif; ?>


              <?php if (!empty($config[telefone3])): ?>
                <div class="telefone_fale">
                  <div class="top10 media">
                    <i class="d-flex fa fa-phone fa-2x" aria-hidden="true"></i>
                    <div class="media-body align-self-center">
                      <h2 class="mt-0 ml-3"><?php Util::imprime($config[ddd3]); ?> <?php Util::imprime($config[telefone3]); ?></h2>
                      </div>
                    </div>
                  </div>
                <?php endif; ?>



                <?php if (!empty($config[telefone4])): ?>
                  <div class="telefone_fale">
                    <div class="top10 media">
                      <i class="d-flex fa fa-phone fa-2x" aria-hidden="true"></i>
                      <div class="media-body align-self-center">
                        <h2 class="mt-0 ml-3"><?php Util::imprime($config[ddd4]); ?> <?php Util::imprime($config[telefone4]); ?></h2>
                        </div>
                      </div>
                    </div>
                  <?php endif; ?>
                  <!-- ======================================================================= -->
                  <!-- telefones  -->
                  <!-- ======================================================================= -->



                </div>

                <!--  ==============================================================  -->
                <!-- FORMULARIO CONTATOS-->
                <!--  ==============================================================  -->
                <div class="col-7 fundo_formulario top75">

                  <div class="form-row">
                    <div class="col">
                      <div class="form-group icon_form">
                        <input type="text" name="nome" class="form-control fundo-form form-control-lg input-lg" placeholder="NOME">
                        <span class="fa fa-user form-control-feedback"></span>
                      </div>
                    </div>

                    <div class="col">
                      <div class="form-group  icon_form">
                        <input type="text" name="email" class="form-control fundo-form form-control-lg input-lg" placeholder="E-MAIL">
                        <span class="fa fa-envelope form-control-feedback"></span>
                      </div>
                    </div>
                  </div>

                  <div class="form-row">
                    <div class="col">
                      <div class="form-group  icon_form">
                        <input type="text" name="telefone" class="form-control fundo-form form-control-lg input-lg" placeholder="TELEFONE">
                        <span class="fa fa-phone form-control-feedback"></span>
                      </div>
                    </div>

                    <div class="col">
                      <div class="form-group  icon_form">
                        <input type="text" name="assunto" class="form-control fundo-form form-control-lg input-lg" placeholder="ASSUNTO">
                        <span class="fa fa-star form-control-feedback"></span>
                      </div>
                    </div>
                  </div>


                  <div class="form-row">
                    <div class="col">
                      <div class="form-group icon_form">
                        <textarea name="mensagem" cols="25" rows="5" class="form-control form-control-lg fundo-form" placeholder="MENSAGEM"></textarea>
                        <span class="fa fa-pencil form-control-feedback"></span>
                      </div>
                    </div>
                  </div>


                  <div class="col-12 text-right padding0 ">
                    <button type="submit" class="btn btn_formulario" name="btn_contato">
                      ENVIAR
                    </button>
                  </div>


                </div>

              </div>
            </form>
            <!--  ==============================================================  -->
            <!-- FORMULARIO CONTATOS-->
            <!--  ==============================================================  -->
          </div>

        </div>
      </div>


      <<div class="container-fluid">
        <div class="row relativo">
          <div class="barra_paginas w-100"></div>
          <div class="container bottom50">
            <div class="row">
              <div class="col mapa">

                <div class=" produtos bottom30 media">
                  <img class="d-flex mr-3" src="<?php echo Util::caminho_projeto() ;?>/imgs/icon_produtos_destaque.png" alt="">
                  <div class="media-body">
                    <h2 class="mt-0"><span>NOSSA LOCALIZAÇÃO</span></h2>
                  </div>
                </div>

                <!-- ======================================================================= -->
                <!-- mapa   -->
                <!-- ======================================================================= -->
                <iframe src="<?php Util::imprime($config[src_place]); ?>" width="100%" height="590" frameborder="0" style="border:0" allowfullscreen></iframe>

                <!-- ======================================================================= -->
                <!-- mapa   -->
                <!-- ======================================================================= -->
              </div>
            </div>
          </div>

        </div>
      </div>



      <!-- ======================================================================= -->
      <!-- rodape    -->
      <!-- ======================================================================= -->
      <?php require_once('./includes/rodape.php') ?>
      <!-- ======================================================================= -->
      <!-- rodape    -->
      <!-- ======================================================================= -->



    </body>

    </html>


    <?php require_once('./includes/js_css.php') ?>


    <script type="text/javascript">
    $(window).load(function() {
      $('.flexslider').flexslider({
        animation: "slide",
        animationLoop: true,
        controlNav: false,/*tira bolinhas*/
        itemWidth: 270,
        itemMargin: 0,
        inItems: 1,
        maxItems: 20
      });
    });
    </script>


    <?php
    //  VERIFICO SE E PARA ENVIAR O EMAIL
    if(isset($_POST[nome]))
    {
      echo $texto_mensagem = "
      Nome: ".($_POST[nome])." <br />
      Email: ".($_POST[email])." <br />
      Telefone: ".($_POST[telefone])." <br />
      Assunto: ".($_POST[assunto])." <br />

      Mensagem: <br />
      ".(nl2br($_POST[mensagem]))."
      ";

      ;




      Util::envia_email($config[email], utf8_decode("$_POST[nome] solicitou contato pelo site"), $texto_mensagem, utf8_decode($_POST[nome]), $_POST[email]);
      Util::envia_email($config[email_copia], utf8_decode("$_POST[nome] solicitou contato pelo site"), $texto_mensagem, utf8_decode($_POST[nome]), $_POST[email]);

      Util::alert_bootstrap("Obrigado por entrar em contato.");
      unset($_POST);
    }


    ?>



    <script>
    $(document).ready(function() {
      $('.FormContatos').bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: {
          valid: 'fa fa-check',
          invalid: 'fa fa-remove',
          validating: 'fa fa-refresh'
        },
        fields: {
          nome: {
            validators: {
              notEmpty: {
                message: 'Insira seu nome.'
              }
            }
          },
          mensagem: {
            validators: {
              notEmpty: {
                message: 'Insira sua Mensagem.'
              }
            }
          },
          email: {
            validators: {
              notEmpty: {
                message: 'Informe um email.'
              },
              emailAddress: {
                message: 'Esse endereço de email não é válido'
              }
            }
          },
          telefone: {
            validators: {
              notEmpty: {
                message: 'Por favor informe seu numero!.'
              },
              phone: {
                country: 'BR',
                message: 'Informe um telefone válido.'
              }
            },
          },
          assunto: {
            validators: {
              notEmpty: {

              }
            }
          }
        }
      });
    });
    </script>
