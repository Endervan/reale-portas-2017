<?php


// INTERNA
$url = Url::getURL(1);


if(!empty($url))
{
  $complemento = "AND url_amigavel = '$url'";
}

$result = $obj_site->select("tb_servicos", $complemento);

if(mysql_num_rows($result)==0)
{
  Util::script_location(Util::caminho_projeto()."/servicos");
}

$dados_dentro = mysql_fetch_array($result);
// BUSCA META TAGS E TITLE
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];


?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",11) ?>
<style>
.bg-interna{
  background:  url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top center no-repeat;
}
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php
  $voltar_para = 'servicos'; // link de volta, exemplo produtos, dicas, servicos etc
  require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->



  <!-- ======================================================================= -->
  <!--  titulo geral -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row ">
      <div class="col-6  text-center top220">
        <h4><span>SERVIÇO</span></h4>
      </div>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!--  titulo -->
  <!-- ======================================================================= -->



  <!-- ======================================================================= -->
  <!-- CATEGORIAS HOME -->
  <!-- ======================================================================= -->
  <div class="container-fluid bg_escuro">
    <div class="row top80">
      <div class="container">
        <div class="row lista_categorias">

          <div class="col-5">
            <div class="media">
              <img class="d-flex mr-3" src="<?php echo Util::caminho_projeto() ;?>/imgs/icon_produtos_destaque.png" alt="">
              <div class="media-body">
                <h2 class="mt-0"><span>FABRICAÇÃO E ASSISTÊNCIA</span></h2>
              </div>
            </div>
          </div>



          <div class="col-7 padding0 top10">
            <div id="slider1" class="flexslider">
              <ul class="slides">

                <li class="active">
                  <a class="btn" href="<?php echo Util::caminho_projeto() ?>/produtos" title="VER TODOS" >
                    VER TODOS
                  </a>
                </li>

                <?php
                $i=0;
                $result = $obj_site->select("tb_categorias_produtos");
                if (mysql_num_rows($result) > 0) {

                  while ($row = mysql_fetch_array($result)) {
                    ?>
                    <li >
                      <a  class="btn" href="<?php echo Util::caminho_projeto() ?>/produtos/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>" >
                        <?php Util::imprime($row[titulo],18); ?>
                      </a>
                    </li>
                    <?php
                  }

                }
                ?>

              </ul>
            </div>
          </div>



        </div>
      </div>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- CATEGORIAS HOME -->
  <!-- ======================================================================= -->



  <div class="container-fluid bg_cinza">
    <div class="row">
      <div class="container">
        <div class="row ">
          <div class="col-6">
            <ol class="breadcrumb endereco_sites top15">
              <li class="breadcrumb-item"><a href="<?php echo $link_topo ; ?>"><i class="fa fa-arrow-circle-o-left fa-2x right5"></i></a></li>
              <li class="breadcrumb-item top5"><a href="<?php echo Util::caminho_projeto() ?>/">HOME</a></li>
              <li class="breadcrumb-item top5"><a href="<?php echo Util::caminho_projeto() ?>/servicos">SERVIÇOS</a></li>
              <li class="breadcrumb-item top5 active"><?php echo Util::imprime($dados_dentro[titulo]) ?></li>
            </ol>
          </div>
          <!--  ==============================================================  -->
          <!--   PESQUISAR -->
          <!--  ==============================================================  -->
          <div class="col-3 procura_empresa">
            <div class="pg5">
              <form action="<?php echo Util::caminho_projeto() ?>/produtos/" method="post">
                <div class="input-group">
                  <input type="text" class="form-control" name="busca_produtos" placeholder="PESQUISAR PRODUTOS" aria-label="PESQUISAR PRODUTOS">
                  <span class="input-group-btn">
                    <button class="btn border-0" type="submit"><span class="fa fa-search"></span></button>
                  </span>
                </div>
              </form>
            </div>
          </div>
          <!--  ==============================================================  -->
          <!--   PESQUISAR -->
          <!--  ==============================================================  -->

          <div class="col-3 top10">
            <a href="<?php echo Util::caminho_projeto() ?>/produtos" class="btn btn-lg btn_orcamento_empresa" title="SOLICITAR UM ORÇAMENTO">
              SOLICITAR ORÇAMENTO
            </a>
          </div>

        </div>
      </div>
    </div>
  </div>




  <!--  ==============================================================  -->
  <!--  DESCRICAO -->
  <!--  ==============================================================  -->
  <div class="container-fluid relativo">
    <div class="row">

      <div class="barra_servicos">  </div>

      <div class="container bottom60">
        <div class="row servico_dentro">

          <div class="col-12 bg_branco top25 pb40">
            <div class="row">
              <div class="col-6 top20">
                <div class="top30">
                  <h5 class="text-uppercase"><?php Util::imprime($dados_dentro[titulo]); ?></h5>
                </div>

                <div class="desc_dica top20">
                  <p><?php Util::imprime($dados_dentro[descricao]); ?></p>
                </div>

                <div class=" text-right top130">
                  <a href="javascript:void(0);" class="btn btn_produtos_orcamento left10" title="SOLICITAR UM ORÇAMENTO" onclick="add_solicitacao(<?php Util::imprime($dados_dentro[0]) ?>, 'servico')" id="btn_add_solicitacao_<?php Util::imprime($dados_dentro[0]) ?>, 'servico'">
                    ADICIONAR AO  ORÇAMENTO<img class="left5 middle" src="<?php echo Util::caminho_projeto() ;?>/imgs/icon_orcamento.png" alt="">
                  </a>
                </div>

              </div>

              <div class="col-6 padding0">
                <div class="top35">
                  <?php $obj_site->redimensiona_imagem("../uploads/$dados_dentro[imagem]",570, 333, array("class"=>"", "alt"=>"$dados_dentro[titulo]")) ?>
                </div>

                <div class="col-9 ml-auto top25">
                  <h1>LIGUE AGORA</h1>
                </div>
                <div class="telefone_prod ">
                  <h5><span class="left40 pt5"><?php Util::imprime($config[ddd1]); ?> <?php Util::imprime($config[telefone1]); ?></span></h5>
                </div>



              </div>

            </div>
          </div>


        </div>
      </div>


    </div>
  </div>
  <!--  ==============================================================  -->
  <!--   DESCRICAO -->
  <!--  ==============================================================  -->




  <!--  ==============================================================  -->
  <!--   PRODUTOS -->
  <!--  ==============================================================  -->
  <div class="container-fluid ">
    <div class="row relativo">
      <div class="barra_paginas w-100"></div>

      <div class="container pb50 top10">
        <div class="row produtos">

          <div class="col-12 ">
            <div class="media">
              <img class="d-flex mr-3" src="<?php echo Util::caminho_projeto() ;?>/imgs/icon_produtos_destaque.png" alt="">
              <div class="media-body">
                <h2 class="mt-0"><span>CONHEÇA NOSSOS PRODUTOS</span></h2>
              </div>
            </div>
          </div>

          <?php require_once('./includes/lista_produtos_paginas.php') ?>


        </div>
      </div>

    </div>
  </div>
  <!--  ==============================================================  -->
  <!--   PRODUTOS -->
  <!--  ==============================================================  -->



  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/rodape.php') ?>
  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->



</body>

</html>




<?php require_once('./includes/js_css.php') ?>


<script type="text/javascript">
$(window).load(function() {
  $('.flexslider').flexslider({
    animation: "slide",
    animationLoop: true,
    controlNav: false,/*tira bolinhas*/
    itemWidth: 230,
    itemMargin: 0,
    inItems: 1,
    maxItems: 20
  });
});
</script>
