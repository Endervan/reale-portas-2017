<?php
// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 1);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>


</head>
<body>



  <div class="topo-site">
    <!-- ======================================================================= -->
    <!-- topo    -->
    <!-- ======================================================================= -->
    <?php require_once('./includes/topo.php') ?>
    <!-- ======================================================================= -->
    <!-- topo    -->
    <!-- ======================================================================= -->
  </div>

  <!-- ======================================================================= -->
  <!-- SLIDER    -->
  <!-- ======================================================================= -->
  <div class="container-fluid relativo">
    <div class="row">

      <div id="container_banner">

        <div id="content_slider">
          <div id="content-slider-1" class="contentSlider rsDefault">

            <?php
            $result = $obj_site->select("tb_banners", "AND tipo_banner = 1 ORDER BY rand() LIMIT 4");

            if (mysql_num_rows($result) > 0) {
              while ($row = mysql_fetch_array($result)) {
                ?>
                <!-- ITEM -->
                <div>
                  <?php
                  if ($row[url] == '') {
                    ?>
                    <img class="rsImg" src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]) ?>" data-rsw="1920" data-rsh="996" alt=""/>
                    <?php
                  }else{
                    ?>
                    <a href="<?php Util::imprime($row[url]) ?>">
                      <img class="rsImg" src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]) ?>" data-rsw="1920" data-rsh="996" alt=""/>
                    </a>
                    <?php
                  }
                  ?>

                </div>
                <!-- FIM DO ITEM -->
                <?php
              }
            }
            ?>

          </div>
        </div>

      </div>

    </div>



    <!-- ======================================================================= -->
    <!-- CATEGORIAS HOME -->
    <!-- ======================================================================= -->
      <div class="row bg_transparente">
        <div class="container">
          <div class="row lista_categorias">
            <div class="col-12 top30">
              <div id="slider1" class="flexslider">
                <ul class="slides">

                  <li class="active">
                    <a class="btn" href="<?php echo Util::caminho_projeto() ?>/produtos" title="" >
                      VER TODOS
                    </a>
                  </li>

                  <?php
                  $i=0;
                  $result = $obj_site->select("tb_categorias_produtos");
                  if (mysql_num_rows($result) > 0) {

                    while ($row = mysql_fetch_array($result)) {
                      ?>
                      <li >
                        <a  class="btn" href="<?php echo Util::caminho_projeto() ?>/produtos/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>" >
                          <?php Util::imprime($row[titulo],18); ?>
                        </a>
                      </li>
                      <?php
                    }

                  }
                  ?>

                </ul>
              </div>
            </div>

          </div>
        </div>
      </div>

    <!-- ======================================================================= -->
    <!-- CATEGORIAS HOME -->
    <!-- ======================================================================= -->


  </div>
  <!-- ======================================================================= -->
  <!-- SLIDER    -->
  <!-- ======================================================================= -->













  <div class="container-fluid bg_produtos_home">
    <div class="row">

      <div class="container">
        <div class="row top20">
          <div class="col-8 ">
            <div class="media">
              <img class="d-flex mr-3" src="<?php echo Util::caminho_projeto() ;?>/imgs/icon_produtos_destaque.png" alt="">
              <div class="media-body">
                <h2 class="mt-0"><span>NOSSOS DESTAQUES</span></h2>
              </div>
            </div>
          </div>
          <!--  ==============================================================  -->
          <!--   PESQUISAR -->
          <!--  ==============================================================  -->
          <div class="col-4 procura">
            <div class="pg5">
              <form action="<?php echo Util::caminho_projeto() ?>/produtos/" method="post">
                <div class="input-group input-group-lg">
                  <input type="text" class="form-control" name="busca_produtos" placeholder="PESQUISAR PRODUTOS" aria-label="PESQUISAR PRODUTOS">
                  <span class="input-group-btn">
                    <button class="btn border-0" type="submit"><span class="fa fa-search"></span></button>
                  </span>
                </div>
              </form>
            </div>
          </div>
          <!--  ==============================================================  -->
          <!--   PESQUISAR -->
          <!--  ==============================================================  -->

        </div>
      </div>

    </div>
  </div>




  <!--  ==============================================================  -->
  <!--   PRODUTOS -->
  <!--  ==============================================================  -->
  <div class="container top10 bottom75">
    <div class="row produtos">
      <?php
      $i = 0;
      $result = $obj_site->select("tb_produtos", "and exibir_home = 'SIM' order by rand() limit 3");
      if(mysql_num_rows($result) == 0){
        echo "<h2 class='bg-info clearfix text-white' style='padding: 20px;'>Nenhum produto encontrado.</h2>";
      }else{
        while ($row = mysql_fetch_array($result))
        {
          $result_categoria = $obj_site->select("tb_categorias_produtos","AND idcategoriaproduto = ".$row[id_categoriaproduto]);
          $row_categoria = mysql_fetch_array($result_categoria);
          ?>
          <div class="col-4 top30">
            <div class="card  text-center">
              <a href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
                <?php //$obj_site->redimensiona_imagem("../uploads/$row[imagem]",176, 316, array("class"=>"text-center imagem", "alt"=>"$row[titulo]")) ?>
                <img width="176" height="316" src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>" alt="<?php Util::imprime($row[url_amigavel]); ?>" class="text-center imagem">
              </a>
              <div class="card-body pt130 text-center">
                <h1 class="card-title text-uppercase"><?php Util::imprime($row_categoria[titulo]); ?></h1>
                <div class="line_text_prod"><h1 class="card-title text-uppercase"><?php Util::imprime($row[titulo],40); ?></h1></div>

                <div class="row top20">
                  <div class="col-12">
                    <a class="btn btn_detalhes_produtos" href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($row[url_amigavel]); ?>"  title="<?php Util::imprime($row[titulo]); ?>">
                      MAIS DETALHES
                    </a>
                    <a href="javascript:void(0);" class="btn btn_produtos_orcamento left10" title="SOLICITAR UM ORÇAMENTO" onclick="add_solicitacao(<?php Util::imprime($row[0]) ?>, 'produto')" id="btn_add_solicitacao_<?php Util::imprime($row[0]) ?>, 'produto'">
                    ORÇAMENTO<img class="left5 middle" src="<?php echo Util::caminho_projeto() ;?>/imgs/icon_orcamento.png" alt="">
                    </a>
                  </div>
                </div>

              </div>
            </div>
          </div>

          <?php
          if($i == 2){
            echo '<div class="clearfix"></div>';
            $i = 0;
          }else{
            $i++;
          }

        }
      }
      ?>

    </div>
  </div>
  <!--  ==============================================================  -->
  <!--   PRODUTOS -->
  <!--  ==============================================================  -->


  <!--  ==============================================================  -->
  <!--   SERVICOS -->
  <!--  ==============================================================  -->
  <div class="container-fluid servicos_home">
    <div class="row">

      <div class="container">
        <div class="row ">
          <div class="col-12 text-right top35">
            <div class="media">
              <div class="media-body">
                <h2 class="mt-0 mb-1"><span>REVENDEDOR REPRESENTANTE COMERCIAL</span></h2>
              </div>
              <img class="d-flex ml-3" src="<?php echo Util::caminho_projeto() ;?>/imgs/icon_servicos_home.png" alt="">
            </div>
          </div>

          <div class="col-12 mt-2 mb-4">
              <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 8);?>
              <p class="top10"><?php echo Util::imprime($row[descricao]) ?></p>
          </div>


        </div>
      </div>


    </div>
  </div>
  <!--  ==============================================================  -->
  <!--   SERVICOS -->
  <!-- ==============================================================  -->


  <!--  ==============================================================  -->
  <!--   EMPRESA HOME -->
  <!--  ==============================================================  -->
  <div class="container-fluid bg_empresa_home">
    <div class="row">

      <div class="container">
        <div class="row top170">
          <div class="col-9 ml-auto">
            <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 1);?>
            <div class="desc_empresa">
              <h3><?php Util::imprime($row[titulo]); ?></h3>
            </div>
          </div>

          <div class="col-8 top40 scrol_empresa">
            <p><?php Util::imprime($row[descricao]); ?></p>
          </div>

          <div class="col-8 ml-auto top20">
            <a class="btn btn_detalhes" href="<?php echo Util::caminho_projeto() ?>/empresa" title="MAIS DETALHES">
              MAIS DETALHES
            </a>
          </div>
        </div>
      </div>

    </div>
  </div>
  <!--  ==============================================================  -->
  <!--   EMPRESA HOME -->
  <!--  ==============================================================  -->

  <!--  ==============================================================  -->
  <!--   VANTAGENS -->
  <!--  ==============================================================  -->
  <div class="container-fluid bg_vantagens">
    <div class="row">

      <div class="container">
        <div class="row">
          <div class="col-12 top20 bottom55">
            <h2><span>VANTAGENS DA REALE PORTAS ACM</span></h2>
          </div>

          <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 3);?>
          <div class="col-12 padding0">
            <div class="col-5 ml-auto top80">
              <p><?php Util::imprime($row[descricao]); ?></p>
            </div>

          </div>


          <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 4);?>
          <div class="col-12 padding0">
            <div class="col-5 ml-auto top80">
              <p><?php Util::imprime($row[descricao]); ?></p>
            </div>
          </div>

          <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 5);?>
          <div class="col-12 padding0">
            <div class="col-5 ml-auto top80">
              <p><?php Util::imprime($row[descricao]); ?></p>
            </div>
          </div>

          <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 6);?>
          <div class="col-12 padding0">
            <div class="col-5 ml-auto top80">
              <p><?php Util::imprime($row[descricao]); ?></p>
            </div>
          </div>

        </div>
      </div>

    </div>
  </div>
  <!--  ==============================================================  -->
  <!--   VANTAGENS -->
  <!--  ==============================================================  -->



  <!--  ==============================================================  -->
  <!--   DICAS -->
  <!--  ==============================================================  -->
  <div class="container-fluid ">
    <div class="row relativo">
      <div class="barra_paginas w-100"></div>

      <div class="container bottom100">
        <div class="row dicas_home">
          <div class="col-12 text-right top35">
            <div class="media">
              <div class="media-body">
                <h2 class="mt-0 mb-1"><span>CONFIRA NOSSAS DICAS</span></h2>
              </div>
              <img class="d-flex ml-3" src="<?php echo Util::caminho_projeto() ;?>/imgs/icon_dicas_home.png" alt="">
            </div>
          </div>


          <?php
          $result = $obj_site->select("tb_dicas"," ORDER BY RAND() limit 3");
          if(mysql_num_rows($result) > 0){
            while($row = mysql_fetch_array($result)){
              $i=0;
              ?>

              <div class="col-4 top40">
                <div class="card bottom50">
                  <a href="<?php echo Util::caminho_projeto() ?>/dica/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
                    <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 370, 268, array("class"=>"card-img-top", "alt"=>"$row[titulo]")) ?>
                  </a>
                  <div class="card-body">
                    <div class="top90 dica_line_titulo">
                      <h3 class="text-uppercase"><span><?php Util::imprime($row[titulo]); ?></span></h3>
                    </div>
                    <div class="dica_line top10">
                      <p class="card-text"><?php Util::imprime($row[descricao]); ?></p>
                    </div>

                    <div class="top20">
                      <a class="btn btn_detalhes_dicas" href="<?php echo Util::caminho_projeto() ?>/dica/<?php Util::imprime($row[url_amigavel]); ?>"  title="<?php Util::imprime($row[titulo]); ?>">
                        MAIS DETALHES
                      </a>
                    </div>

                  </div>
                </div>
              </div>

              <?php
              if ($i==2) {
                echo "<div class='clearfix'>  </div>";
              }else {
                $i++;
              }
            }
          }
          ?>

        </div>
      </div>

    </div>
  </div>
  <!--  ==============================================================  -->
  <!--   DICAS -->
  <!--  ==============================================================  -->



  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/rodape.php') ?>
  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->

</body>

</html>

<!-- slider JS files -->
<script  src="<?php echo Util::caminho_projeto() ?>/js/jquery-1.8.0.min.js"></script>
<link href="<?php echo Util::caminho_projeto() ?>/css/royalslider.css" rel="stylesheet">
<script src="<?php echo Util::caminho_projeto() ?>/js/jquery.royalslider.min.js"></script>
<link href="<?php echo Util::caminho_projeto() ?>/css/rs-minimal-white.css" rel="stylesheet">



<script>
jQuery(document).ready(function($) {
  // Please note that autoHeight option has some conflicts with options like imageScaleMode, imageAlignCenter and autoScaleSlider
  // it's recommended to disable them when using autoHeight module
  $('#content-slider-1').royalSlider({
    autoHeight: true,
    arrowsNav: true,
    arrowsNavAutoHide: false,
    keyboardNavEnabled: true,
    controlNavigationSpacing: 0,
    controlNavigation: 'tabs',
    autoScaleSlider: false,
    arrowsNavAutohide: true,
    arrowsNavHideOnTouch: true,
    imageScaleMode: 'none',
    globalCaption:true,
    imageAlignCenter: false,
    fadeinLoadedSlide: true,
    loop: false,
    loopRewind: true,
    numImagesToPreload: 6,
    keyboardNavEnabled: true,
    usePreloader: false,
    autoPlay: {
      // autoplay options go gere
      enabled: true,
      pauseOnHover: true
    }

  });
});
</script>





<?php require_once('./includes/js_css.php') ?>


<script type="text/javascript">
$(window).load(function() {
  $('.flexslider').flexslider({
    animation: "slide",
    animationLoop: true,
    itemWidth: 230,
    itemMargin: 0,
    inItems: 1,
    maxItems: 20
  });
});
</script>
