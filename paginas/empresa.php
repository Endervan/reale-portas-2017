<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 2);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",1) ?>
<style>
.bg-interna{
  background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top center no-repeat;
}
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php
  $voltar_para = ''; // link de volta, exemplo produtos, dicas, servicos etc
  require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->



  <!-- ======================================================================= -->
  <!--  titulo geral -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row ">
      <div class="col-6  text-center top220">
        <h4><span>A EMPRESA</span></h4>
      </div>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!--  titulo -->
  <!-- ======================================================================= -->



  <!-- ======================================================================= -->
  <!-- CATEGORIAS HOME -->
  <!-- ======================================================================= -->
  <div class="container-fluid bg_escuro">
    <div class="row top80">
      <div class="container">
        <div class="row lista_categorias">

          <div class="col-3">
            <div class="media">
              <img class="d-flex mr-3" src="<?php echo Util::caminho_projeto() ;?>/imgs/icon_produtos_destaque.png" alt="">
              <div class="media-body">
                <h2 class="mt-0"><span>QUEM SOMOS</span></h2>
              </div>
            </div>
          </div>

          <div class="col-9 top10">
            <div id="slider1" class="flexslider">
              <ul class="slides">

                <li class="active">
                  <a class="btn" href="<?php echo Util::caminho_projeto() ?>/produtos" title="VER TODOS" >
                    VER TODOS
                  </a>
                </li>

                <?php
                $i=0;
                $result = $obj_site->select("tb_categorias_produtos");
                if (mysql_num_rows($result) > 0) {

                  while ($row = mysql_fetch_array($result)) {
                    ?>
                    <li >
                      <a  class="btn" href="<?php echo Util::caminho_projeto() ?>/produtos/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>" >
                        <?php Util::imprime($row[titulo],18); ?>
                      </a>
                    </li>
                    <?php
                  }

                }
                ?>

              </ul>
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- CATEGORIAS HOME -->
  <!-- ======================================================================= -->


  <div class="container-fluid bg_cinza">
    <div class="row">
      <div class="container">
        <div class="row ">
          <div class="col-6">
            <ol class="breadcrumb endereco_sites top15">
              <li class="breadcrumb-item"><a href="<?php echo $link_topo ; ?>"><i class="fa fa-arrow-circle-o-left fa-2x right5"></i></a></li>
              <li class="breadcrumb-item top5"><a href="<?php echo Util::caminho_projeto() ?>/">HOME</a></li>
              <li class="breadcrumb-item top5 active">EMPRESA</li>
            </ol>
          </div>
          <!--  ==============================================================  -->
          <!--   PESQUISAR -->
          <!--  ==============================================================  -->
          <div class="col-3 procura_empresa">
            <div class="pg5">
              <form action="<?php echo Util::caminho_projeto() ?>/produtos/" method="post">
                <div class="input-group">
                  <input type="text" class="form-control" name="busca_produtos" placeholder="PESQUISAR PRODUTOS" aria-label="PESQUISAR PRODUTOS">
                  <span class="input-group-btn">
                    <button class="btn border-0" type="submit"><span class="fa fa-search"></span></button>
                  </span>
                </div>
              </form>
            </div>
          </div>
          <!--  ==============================================================  -->
          <!--   PESQUISAR -->
          <!--  ==============================================================  -->

          <div class="col-3 top10">
            <a href="<?php echo Util::caminho_projeto() ?>/produtos" class="btn btn-lg btn_orcamento_empresa" title="SOLICITAR UM ORÇAMENTO">
              SOLICITAR ORÇAMENTO
            </a>
          </div>

        </div>
      </div>
    </div>








    <!--  ==============================================================  -->
    <!--   EMPRESA HOME -->
    <!--  ==============================================================  -->
    <div class="container-fluid bg_empresa">
      <div class="row">

        <div class="container">
          <div class="row top170">
            <div class="col-8 ml-auto">
              <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 2);?>
              <div class="">
                <h3><?php Util::imprime($row[titulo]); ?></h3>
              </div>
            </div>

            <div class="col-8 top40 scrol_empresa01">
              <p><?php Util::imprime($row[descricao]); ?></p>
            </div>

          </div>
        </div>

      </div>
    </div>
    <!--  ==============================================================  -->
    <!--   EMPRESA HOME -->
    <!--  ==============================================================  -->



    <!--  ==============================================================  -->
    <!--   PRODUTOS -->
    <!--  ==============================================================  -->
    <div class="container-fluid ">
      <div class="row relativo">
        <div class="barra_paginas w-100"></div>

        <div class="container pb50 top10">
          <div class="row produtos">

            <div class="col-12 ">
              <div class="media">
                <img class="d-flex mr-3" src="<?php echo Util::caminho_projeto() ;?>/imgs/icon_produtos_destaque.png" alt="">
                <div class="media-body">
                  <h2 class="mt-0"><span>CONHEÇA NOSSOS PRODUTOS</span></h2>
                </div>
              </div>
            </div>

            <?php require_once('./includes/lista_produtos_paginas.php') ?>

          </div>
        </div>

      </div>
    </div>
    <!--  ==============================================================  -->
    <!--   PRODUTOS -->
    <!--  ==============================================================  -->

    <!-- ======================================================================= -->
    <!-- rodape    -->
    <!-- ======================================================================= -->
    <?php require_once('./includes/rodape.php') ?>
    <!-- ======================================================================= -->
    <!-- rodape    -->
    <!-- ======================================================================= -->



  </body>

  </html>


  <?php require_once('./includes/js_css.php') ?>

  <script type="text/javascript">
  $(window).load(function() {
    $('.flexslider').flexslider({
      animation: "slide",
      animationLoop: true,
      controlNav: false,/*tira bolinhas*/
      itemWidth: 215,
      itemMargin: 0,
      inItems: 1,
      maxItems: 20
    });
  });
  </script>
