<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 6);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

//  EXCLUI UM ITEM
if(isset($_GET[action]))
{
  //  SELECIONO O TIPO
  switch($_GET[tipo])
  {
    case "produto":
    $id = $_GET[id];
    unset($_SESSION[solicitacoes_produtos][$id]);
    sort($_SESSION[solicitacoes_produtos]);
    break;
    case "servico":
    $id = $_GET[id];
    unset($_SESSION[solicitacoes_servicos][$id]);
    sort($_SESSION[solicitacoes_servicos]);
    break;
    case "piscina_vinil":
    $id = $_GET[id];
    unset($_SESSION[piscina_vinil][$id]);
    sort($_SESSION[piscina_vinil]);
    break;
  }

}


?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",7) ?>
<style>
.bg-interna{
  background:  url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top center no-repeat;
  z-index: 999 !important;
}
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->



  <!-- ======================================================================= -->
  <!--  titulo geral -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row ">
      <div class="col-6  text-center top220">
        <h4><span>ORÇAMENTO</span></h4>
      </div>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!--  titulo -->
  <!-- ======================================================================= -->



  <!-- ======================================================================= -->
  <!-- CATEGORIAS HOME -->
  <!-- ======================================================================= -->
  <div class="container-fluid bg_escuro">
    <div class="row top80">
      <div class="container">
        <div class="row lista_categorias">

          <div class="col-4">
            <div class="media">
              <img class="d-flex mr-3" src="<?php echo Util::caminho_projeto() ;?>/imgs/icon_produtos_destaque.png" alt="">
              <div class="media-body">
                <h2 class="mt-0"><span>SOLICITE AGORA</span></h2>
              </div>
            </div>
          </div>

          <div class="col-8 padding0 top10">
            <div id="slider1" class="flexslider">
              <ul class="slides">

                <li class="active">
                  <a class="btn" href="<?php echo Util::caminho_projeto() ?>/produtos" title="VER TODOS" >
                    VER TODOS
                  </a>
                </li>

                <?php
                $i=0;
                $result = $obj_site->select("tb_categorias_produtos");
                if (mysql_num_rows($result) > 0) {

                  while ($row = mysql_fetch_array($result)) {
                    ?>
                    <li >
                      <a  class="btn" href="<?php echo Util::caminho_projeto() ?>/produtos/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>" >
                        <?php Util::imprime($row[titulo],18); ?>
                      </a>
                    </li>
                    <?php
                  }

                }
                ?>

              </ul>
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- CATEGORIAS HOME -->
  <!-- ======================================================================= -->


  <div class="container-fluid bg_cinza">
    <div class="row">
      <div class="container">
        <div class="row ">
          <div class="col-4">
            <ol class="breadcrumb endereco_sites top15">
              <li class="breadcrumb-item"><a href="<?php echo $link_topo ; ?>"><i class="fa fa-arrow-circle-o-left fa-2x right5"></i></a></li>
              <li class="breadcrumb-item top5"><a href="<?php echo Util::caminho_projeto() ?>/">HOME</a></li>
              <li class="breadcrumb-item top5 active">FALE CONOSCO</li>
            </ol>
          </div>

          <!-- ======================================================================= -->
          <!-- menu -->
          <!-- ======================================================================= -->
          <div class="col-8 top25 menu_contatos">
            <ul class="nav nav-pills nav-fill text-center">
              <li class="nav-item">
                <a class="nav-link" href="<?php echo Util::caminho_projeto() ?>/contatos">
                  FALE CONOSCO
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#"  onclick="$('html,body').animate({scrollTop: $('.mapa').offset().top}, 1000);">
                  ONDE ESTAMOS
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="<?php echo Util::caminho_projeto() ?>/trabalhe-conosco">
                  TRABALHE CONOSCO
                </a>
              </li>
            </ul>
          </div>
          <!-- ======================================================================= -->
          <!-- menu -->
          <!-- ======================================================================= -->


        </div>
      </div>
    </div>
  </div>




  <div class="container-fluid bg_fale ">
    <div class="row">

      <div class="container top40 relativo">
        <form class="FormContatos was-validated" role="form" method="post" enctype="multipart/form-data">
          <div class="row ">

            <div class="col-4">
              <!-- ======================================================================= -->
              <!-- CARRINHO  -->
              <!-- ======================================================================= -->
            <?php require_once('./includes/lista_itens_orcamento.php') ?>
              <!-- ======================================================================= -->
              <!-- FORMULARIO  -->
              <!-- ======================================================================= -->

              <div class=" top15">
                <h6>SE PREFERIR, LIGUE AGORA:</h6>
              </div>


              <!-- ======================================================================= -->
              <!-- telefones  -->
              <!-- ======================================================================= -->
              <div class="telefone_fale top20">
                <div class="media">
                  <img class="d-flex" src="<?php echo Util::caminho_projeto() ?>/imgs/icon_telefone_rodape.png" alt="">
                  <div class="media-body align-self-center">
                    <h2 class="mt-0 ml-3"><?php Util::imprime($config[ddd1]); ?> <?php Util::imprime($config[telefone1]); ?></h2>
                  </div>
                </div>
              </div>


              <?php if (!empty($config[telefone2])): ?>
                <div class="telefone_fale">
                  <div class="top10 media">
                    <img class="d-flex" src="<?php echo Util::caminho_projeto() ?>/imgs/icon_whats_rodape.png" alt="">
                    <div class="media-body align-self-center">
                      <h2 class="mt-0 ml-3"><?php Util::imprime($config[ddd2]); ?> <?php Util::imprime($config[telefone2]); ?></h2>
                    </div>
                  </div>
                </div>
              <?php endif; ?>


            </div>


              <div class="col-7">
                <div class="fundo_formulario">
                  <h3 class="top20">CONFIRME SEUS DADOS</h3>


                  <div class="form-row">
                    <div class="form-group  col icon_form">
                      <input type="text" name="nome" class="form-control fundo-form form-control-lg" placeholder="NOME"  required>
                      <span class="fa fa-user form-control-feedback"></span>
                    </div>
                    <div class="form-group col icon_form">
                      <input type="email" name="email" class="form-control fundo-form form-control-lg"  placeholder="EMAIL" required>
                      <span class="fa fa-envelope form-control-feedback"></span>
                    </div>
                  </div>

                  <div class="form-row">
                    <div class="form-group col icon_form">
                      <input type="tel" id="phone" title='preenchar Telefone válido(fixo ou celular)' name="telefone" class="form-control fundo-form form-control-lg" placeholder="TELEFONE"  required>
                      <span class="fa fa-phone form-control-feedback"></span>
                    </div>
                    <div class="form-group col icon_form">
                      <input type="text" name="localidade" class="form-control fundo-form form-control-lg"  placeholder="LOCALIDADE" required>
                      <span class="fa fa-home form-control-feedback"></span>
                    </div>
                  </div>

                  <div class="form-row">
                    <div class="col">
                      <div class="form-group icon_form">
                        <textarea name="mensagem" cols="25" rows="5" class="form-control form-control-lg fundo-form" placeholder="MENSAGEM"></textarea>
                        <span class="fa fa-pencil form-control-feedback"></span>
                      </div>
                    </div>
                  </div>


                  <div class="col-12 text-right  top15 padding0">
                    <button type="submit" class="btn btn_formulario" name="btn_contato">
                      ENVIAR
                    </button>
                  </div>

                </div>

              </div>
              <!--  ==============================================================  -->
              <!-- FORMULARIO-->
              <!--  ==============================================================  -->

            </div>
          </form>




        </div>

      </div>
    </div>


    <<div class="container-fluid">
      <div class="row relativo">
        <div class="barra_paginas w-100"></div>
        <div class="container bottom50">
          <div class="row">
            <div class="col mapa">

              <div class=" produtos bottom30 media">
                <img class="d-flex mr-3" src="<?php echo Util::caminho_projeto() ;?>/imgs/icon_produtos_destaque.png" alt="">
                <div class="media-body">
                  <h2 class="mt-0"><span>NOSSA LOCALIZAÇÃO</span></h2>
                </div>
              </div>

              <!-- ======================================================================= -->
              <!-- mapa   -->
              <!-- ======================================================================= -->
              <iframe src="<?php Util::imprime($config[src_place]); ?>" width="100%" height="590" frameborder="0" style="border:0" allowfullscreen></iframe>

              <!-- ======================================================================= -->
              <!-- mapa   -->
              <!-- ======================================================================= -->
            </div>
          </div>
        </div>

      </div>
    </div>



    <!-- ======================================================================= -->
    <!-- rodape    -->
    <!-- ======================================================================= -->
    <?php require_once('./includes/rodape.php') ?>
    <!-- ======================================================================= -->
    <!-- rodape    -->
    <!-- ======================================================================= -->



  </body>

  </html>


  <?php require_once('./includes/js_css.php') ?>


  <script type="text/javascript">
  $(window).load(function() {
    $('.flexslider').flexslider({
      animation: "slide",
      animationLoop: true,
      controlNav: false,/*tira bolinhas*/
      itemWidth: 270,
      itemMargin: 0,
      inItems: 1,
      maxItems: 20
    });
  });
  </script>


  <?php
  //  VERIFICO SE E PARA CADASTRAR A SOLICITACAO
  if(isset($_POST[nome])){



    //  CADASTRO OS PRODUTOS SOLICITADOS
    for($i=0; $i < count($_POST[qtd]); $i++){
      $dados = $obj_site->select_unico("tb_produtos", "idproduto", $_POST[idproduto][$i]);

      $produtos .= "
      <tr>
      <td><p>". $_POST[qtd][$i] ."</p></td>
      <td><p>". utf8_encode($dados[titulo]) ."</p></td>
      </tr>
      ";
    }

    //  CADASTRO OS SERVICOS SOLICITADOS
    for($i=0; $i < count($_POST[qtd_servico]); $i++){
      $dados = $obj_site->select_unico("tb_servicos", "idservico", $_POST[idservico][$i]);
      $produtos .= "
      <tr>
      <td><p>". $_POST[qtd_servico][$i] ."</p></td>
      <td><p>". utf8_encode($dados[titulo]) ."</p></td>
      </tr>
      ";
    }




    //  ENVIANDO A MENSAGEM PARA O CLIENTE
    $texto_mensagem = "
    O seguinte cliente fez uma solicitação pelo site. <br />

    Nome: $_POST[nome] <br />
    Email: $_POST[email] <br />
    Telefone: $_POST[telefone] <br />
    Assunto: $_POST[assunto] <br />
    Mensagem: <br />
    ". nl2br($_POST[mensagem]) ." <br />

    <br />
    <h2> Produtos selecionados:</h2> <br />

    <table width='100%' border='0' cellpadding='5' cellspacing='5'>
    <tr>
    <td><h4>QTD</h4></td>
    <td><h4>PRODUTO</h4></td>
    </tr>
    $produtos
    </table>

    ";




    Util::envia_email($config[email], utf8_decode("$_POST[nome] solicitou um orçamento"), $texto_mensagem, $nome_remetente, $email);
    Util::envia_email($config[email_copia], utf8_decode("$_POST[nome] solicitou um orçamento"), $texto_mensagem, $nome_remetente, $email);
    unset($_SESSION[solicitacoes_produtos]);
    unset($_SESSION[solicitacoes_servicos]);
    unset($_SESSION[piscinas_vinil]);
    Util::alert_bootstrap("Orçamento enviado com sucesso. Em breve entraremos em contato.");

  }
  ?>
