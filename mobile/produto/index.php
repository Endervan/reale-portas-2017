
<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// INTERNA
$url =$_GET[get1];

if(!empty($url)){
	$complemento = "AND url_amigavel = '$url'";
}

$result = $obj_site->select("tb_produtos", $complemento);

if(mysql_num_rows($result)==0){
	Util::script_location(Util::caminho_projeto()."/mobile/produtos/");
}

$dados_dentro = mysql_fetch_array($result);
// BUSCA META TAGS E TITLE
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>


<!doctype html>
<html amp lang="pt-br">
<head>
	<?php require_once("../includes/head.php"); ?>
	<script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>

	<style amp-custom>
	<?php require_once("../css/geral.css"); ?>
	<?php require_once("../css/topo_rodape.css"); ?>
	<?php require_once("../css/paginas.css");  //  ARQUIVO DA PAGINA ?>



	<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 13); ?>
	.bg-interna{
		background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 67px center  no-repeat;
	}
	</style>

	<script async custom-element="amp-bind" src="https://cdn.ampproject.org/v0/amp-bind-0.1.js"></script>
	<script async custom-element="amp-fit-text" src="https://cdn.ampproject.org/v0/amp-fit-text-0.1.js"></script>
	<script async custom-element="amp-image-lightbox" src="https://cdn.ampproject.org/v0/amp-image-lightbox-0.1.js"></script>
	<script async custom-element="amp-carousel" src="https://cdn.ampproject.org/v0/amp-carousel-0.1.js"></script>
	<script async custom-element="amp-lightbox" src="https://cdn.ampproject.org/v0/amp-lightbox-0.1.js"></script>






</head>

<body class="bg-interna">


	<?php
	$voltar_para = 'produtos'; // link de volta, exemplo produtos, dicas, servicos etc
	require_once("../includes/topo.php") ?>

	<div class="row">

		<div class="col-12 top50 bottom50 text-center titulo_emp">
			<!-- ======================================================================= -->
			<!-- TITULO PAGINA  -->
			<!-- ======================================================================= -->
			<h6>PRODUTO</h6>
			<amp-img class="top20 " src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/barra_titulo.png" alt="" height="5" width="90"></amp-img>
			<!-- ======================================================================= -->
			<!-- TITULO PAGINA  -->
			<!-- ======================================================================= -->
		</div>
	</div>





	<div class="row relativo">
		<div class="col-12 bg_escuro text-center">

			<div class="col-12 padding0 barra_produto">
				<amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/barra_dica.png"
					width="214"
					height="80"
					layout="responsive"
					alt="AMP">
				</amp-img>
			</div>


			<?php
			$result = $obj_site->select("tb_galerias_produtos", "AND id_produto = '$dados_dentro[0]' ");
			if(mysql_num_rows($result) == 0){ ?>

				<amp-carousel id="carousel-with-preview"
				width="480"
				height="400"
				layout="responsive"
				type="slides"
				autoplay
				delay="4000"
				>

				<amp-img
				on="tap:lightbox2"
				role="button"
				tabindex="0"
				src="<?php echo Util::caminho_projeto() ?>/imgs/default.png"
				layout="responsive"
				width="480"
				height="400"
				alt="default">

			</amp-img>
		</amp-carousel>

		<amp-image-lightbox id="lightbox2"layout="nodisplay">	</amp-image-lightbox>

	<?php }else{?>
		<amp-carousel id="carousel-with-preview"
		width="480"
		height="400"
		layout="responsive"
		type="slides"
		autoplay
		delay="4000"
		>

		<?php
		$i = 0;
		$result = $obj_site->select("tb_galerias_produtos", "and id_produto = $dados_dentro[idproduto]");
		if (mysql_num_rows($result) > 0) {
			while($row = mysql_fetch_array($result)){
				?>


				<amp-img
				on="tap:lightbox1"
				role="button"
				tabindex="0"

				src="<?php echo Util::caminho_projeto() ?>/uploads/<?php echo Util::imprime($row[imagem]) ?>"
				layout="responsive"
				width="480"
				height="400"
				alt="<?php echo Util::imprime($row[titulo]) ?>">

			</amp-img>
			<?php
		}
		$i++;
	}


	?>

</amp-carousel>

<amp-image-lightbox id="lightbox1"layout="nodisplay">	</amp-image-lightbox>

<div class="carousel-preview text-center">

	<?php
	$i = 0;
	$result = $obj_site->select("tb_galerias_produtos", "and id_produto = $dados_dentro[idproduto]");
	if (mysql_num_rows($result) > 0) {
		while($row = mysql_fetch_array($result)){
			?>

			<button class="rounded-circle left5" on="tap:carousel-with-preview.goToSlide(index=<?php echo $i++; ?>)">

				<amp-img class="rounded-circle"
				src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/bolinhas.png"
				width="20"
				height="20"
				alt="<?php echo Util::imprime($row[titulo]) ?>">
			</amp-img>
		</button>

		<?php
	}
	$i++;
}


?>
</div>

<?php } ?>

</div>

</div>


<!-- ======================================================================= -->
<!--  CONTATOS -->
<!-- ======================================================================= -->
<div class="row bg_cinza_escuro top_Futuante6 relativo">

	<div class="col-6">
		<a href="tel:+55<?php Util::imprime($config[ddd1]); ?><?php Util::imprime($config[telefone1]); ?>">
			<amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/telefone_dicas.png"
				width="214"
				height="50"
				layout="responsive"
				alt="AMP">
			</amp-img>
		</a>

		<div class="col-6 dicas">
			<h2>
				<amp-fit-text
				width="240"
				height="30"
				layout="responsive">
				<?php Util::imprime($config[ddd1]); ?><?php Util::imprime($config[telefone1]); ?>
			</amp-fit-text>
		</h2>
	</div>
</div>

<div class="col-6">
	<a
	on="tap:my-lightbox444"
	role="a"
	tabindex="0">
	<amp-img
	layout="responsive"
	src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/filtro_produtos.png" alt="" height="50" width="220">
</amp-img>

</a>
<amp-lightbox id="my-lightbox444" layout="nodisplay">
<div class="lightbox" role="a" tabindex="0">
	<!-- ======================================================================= -->
	<!-- MENU CATEGORIAS  -->
	<!-- ======================================================================= -->
	<div class="">
		<a class="btn btn_fechar" on="tap:my-lightbox.close">X</a>
		<?php require_once("../includes/menu_produtos.php"); ?>
	</div>
	<!-- ======================================================================= -->
	<!-- MENU CATEGORIAS  -->
	<!-- ======================================================================= -->
</div>
</amp-lightbox>
</div>


</div>
<!-- ======================================================================= -->
<!--  CONTATOS -->
<!-- ======================================================================= -->



<!-- ======================================================================= -->
<!--  PRODUTOS DETALHES -->
<!-- ======================================================================= -->
<div class="row bg_cinza bottom20">
	<div class="col-12 dicas_dentro top20">
		<h1 class="text-uppercase"><?php echo Util::imprime($dados_dentro[titulo]) ?></h1>
	</div>

	<div class="col-12 dicas_dentro">
		<h3><span><?php Util::imprime( Util::troca_value_nome($dados_dentro[id_categoriaproduto], "tb_categorias_produtos", "idcategoriaproduto", "titulo") ); ?></span></h3>
	</div>

	<div class="col-12 bottom50 top20">
		<p ><span><?php echo Util::imprime($dados_dentro[descricao]) ?></span></p>
	</div>

	<div class="col-6 col-offset-6 orcamento_servico">
		<a href="<?php echo Util::caminho_projeto() ?>/mobile/orcamento/index.php?action=add&id=<?php echo $dados_dentro[0] ?>&tipo_orcamento=produto" title="SOLICITAR UM ORÇAMENTO">
			<amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/orcamento_servico.png"
				width="224"
				height="50"
				layout="responsive"
				alt="AMP">
			</amp-img>
		</a>
	</div>

</div>
<!-- ======================================================================= -->
<!--  PRODUTOS DETALHES -->
<!-- ======================================================================= -->


<!-- ======================================================================= -->
<!--  PRODUTOS E SERVICOS -->
<!-- ======================================================================= -->
<div class="row bottom50">
	<div class="col-11 top40">

		<amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/produtos_servicos.png"
			width="350"
			height="79"
			layout="responsive"
			alt="AMP">
		</amp-img>

	</div>

	<div class="clearfix">  </div>

	<div class="col-4 top20">
		<a href="<?php echo Util::caminho_projeto(); ?>/mobile/produtos">
			<amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/icon_produtos_geral.png"
				width="125"
				height="125"
				layout="responsive"
				alt="AMP">
			</amp-img>
		</a>
	</div>

	<div class="col-4 top20">
		<a href="<?php echo Util::caminho_projeto(); ?>/mobile/servicos">
			<amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/icon_servicos_geral.png"
				width="125"
				height="125"
				layout="responsive"
				alt="AMP">
			</amp-img>
		</a>
	</div>


	<div class="col-4 top20">
		<a href="<?php echo Util::caminho_projeto(); ?>/mobile/orcamento">
			<amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/icon_orcamento_geral.png"
				width="125"
				height="125"
				layout="responsive"
				alt="AMP">
			</amp-img>
		</a>
	</div>



</div>
<!-- ======================================================================= -->
<!--  PRODUTOS E SERVICOS -->
<!-- ======================================================================= -->



<?php require_once("../includes/rodape.php") ?>

</body>



</html>
