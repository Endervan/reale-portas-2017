
<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// INTERNA
$url =$_GET[get1];

if(!empty($url)){
	$complemento = "AND url_amigavel = '$url'";
}

$result = $obj_site->select("tb_dicas", $complemento);

if(mysql_num_rows($result)==0){
	Util::script_location(Util::caminho_projeto()."/mobile/dicas/");
}

$dados_dentro = mysql_fetch_array($result);
// BUSCA META TAGS E TITLE
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>


<!doctype html>
<html amp lang="pt-br">
<head>
  <?php require_once("../includes/head.php"); ?>
  <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>

  <style amp-custom>
  <?php require_once("../css/geral.css"); ?>
  <?php require_once("../css/topo_rodape.css"); ?>
  <?php require_once("../css/paginas.css");  //  ARQUIVO DA PAGINA ?>



  <?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 15); ?>
  .bg-interna{
    background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 67px center  no-repeat;
  }
  </style>

  <script async custom-element="amp-bind" src="https://cdn.ampproject.org/v0/amp-bind-0.1.js"></script>
  <script async custom-element="amp-fit-text" src="https://cdn.ampproject.org/v0/amp-fit-text-0.1.js"></script>
	<script async custom-element="amp-image-lightbox" src="https://cdn.ampproject.org/v0/amp-image-lightbox-0.1.js"></script>





</head>

<body class="bg-interna">


  <?php
  $voltar_para = 'dicas'; // link de volta, exemplo produtos, dicas, servicos etc
  require_once("../includes/topo.php") ?>

  <div class="row">

    <div class="col-12 top50 bottom50 text-center titulo_emp">
      <!-- ======================================================================= -->
      <!-- TITULO PAGINA  -->
      <!-- ======================================================================= -->
      <h6>DICA</h6>
      <amp-img class="top20 " src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/barra_titulo.png" alt="" height="5" width="90"></amp-img>
      <!-- ======================================================================= -->
      <!-- TITULO PAGINA  -->
      <!-- ======================================================================= -->
    </div>
  </div>

  <!-- ======================================================================= -->
  <!--  barra titulo -->
  <!-- ======================================================================= -->
  <div class="row relativo top10">

		<div class="col-12">
			<amp-img
			on="tap:lightbox1"
			role="button"
			tabindex="0"

			src="<?php echo Util::caminho_projeto() ?>/uploads/<?php echo Util::imprime($dados_dentro[imagem]) ?>"
			layout="responsive"
			width="480"
			height="250"
			alt="<?php echo Util::imprime($dados_dentro[titulo]) ?>">

		</amp-img>
		<amp-image-lightbox id="lightbox1"layout="nodisplay">	</amp-image-lightbox>

	</div>


    <div class="col-9 col-offset-3 ">
      <div class="dicas_dentro_desc">	<h2 class="top10"><span><?php echo Util::imprime($dados_dentro[titulo]) ?></span></h2></div>
    </div>
    <div class="col-12 padding0 dica_barra">
      <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/barra_dica.png"
        width="214"
        height="100"
        layout="responsive"
        alt="AMP">
      </amp-img>
    </div>

  </div>
  <!-- ======================================================================= -->
  <!--  barra titulo -->
  <!-- ======================================================================= -->


  <!-- ======================================================================= -->
  <!--  CONTATOS -->
  <!-- ======================================================================= -->
  <div class="row top30 bg_cinza_escuro relativo">

    <div class="col-6">
      <a href="tel:+55<?php Util::imprime($config[ddd1]); ?><?php Util::imprime($config[telefone1]); ?>">
        <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/telefone_dicas.png"
          width="214"
          height="50"
          layout="responsive"
          alt="AMP">
        </amp-img>
      </a>

      <div class="col-6 dicas">
        <h2>
          <amp-fit-text
          width="240"
          height="30"
          layout="responsive">
          <?php Util::imprime($config[ddd1]); ?><?php Util::imprime($config[telefone1]); ?>
        </amp-fit-text>
      </h2>
    </div>
  </div>


  <div class="col-6">
    <a href="<?php echo Util::caminho_projeto(); ?>/mobile/orcamento">
      <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/orcamento.png"
        width="224"
        height="50"
        layout="responsive"
        alt="AMP">
      </amp-img>
    </a>
  </div>



</div>
<!-- ======================================================================= -->
<!--  CONTATOS -->
<!-- ======================================================================= -->

<!-- ======================================================================= -->
<!--  DICAS DETALHE -->
<!-- ======================================================================= -->
<div class="row bg_cinza">
  <div class="col-12 dicas_dentro">
    <p ><?php echo Util::imprime($dados_dentro[descricao]) ?></p>
  </div>
</div>
<!-- ======================================================================= -->
<!--  DICAS DETALHE -->
<!-- ======================================================================= -->


<!-- ======================================================================= -->
<!--  PRODUTOS E SERVICOS -->
<!-- ======================================================================= -->
<div class="row bottom50">
  <div class="col-11 top40">

    <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/produtos_servicos.png"
      width="350"
      height="79"
      layout="responsive"
      alt="AMP">
    </amp-img>

  </div>

  <div class="clearfix">  </div>

  <div class="col-4 top20">
    <a href="<?php echo Util::caminho_projeto(); ?>/mobile/produtos">
      <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/icon_produtos_geral.png"
        width="125"
        height="125"
        layout="responsive"
        alt="AMP">
      </amp-img>
    </a>
  </div>

  <div class="col-4 top20">
    <a href="<?php echo Util::caminho_projeto(); ?>/mobile/servicos">
      <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/icon_servicos_geral.png"
        width="125"
        height="125"
        layout="responsive"
        alt="AMP">
      </amp-img>
    </a>
  </div>


  <div class="col-4 top20">
    <a href="<?php echo Util::caminho_projeto(); ?>/mobile/orcamento">
      <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/icon_orcamento_geral.png"
        width="125"
        height="125"
        layout="responsive"
        alt="AMP">
      </amp-img>
    </a>
  </div>



</div>
<!-- ======================================================================= -->
<!--  PRODUTOS E SERVICOS -->
<!-- ======================================================================= -->



<?php require_once("../includes/rodape.php") ?>

</body>



</html>
