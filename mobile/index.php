
<?php
require_once("../class/Include.class.php");
$obj_site = new Site();
?>
<!doctype html>
<html amp lang="pt-br">
<head>
  <?php require_once("./includes/head.php"); ?>

  <style amp-custom>
  <?php require_once("./css/geral.css"); ?>
  <?php require_once("./css/topo_rodape.css"); ?>
  <?php require_once("./css/paginas.css");  //  ARQUIVO DA PAGINA ?>
  </style>

  <script async custom-element="amp-fit-text" src="https://cdn.ampproject.org/v0/amp-fit-text-0.1.js"></script>



</head>



<body class="bg-index">


  <div class="row">
    <div class="col-12 text-center topo">
      <amp-img layout="responsive" src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/logo.png" alt="Home" height="66" width="480"></amp-img>
    </div>
  </div>


  <div class="row font-index">

    <div class="col-12">
        <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/texto_home.png"
          width="450"
          class="teste"
          height="101"
          layout="responsive"
          alt="AMP">
        </amp-img>
    </div>

    <!-- ======================================================================= -->
    <!--  PRODUTOS E SERVICOS -->
    <!-- ======================================================================= -->
    <div class="row">

      <div class="col-4 top20">
        <a href="<?php echo Util::caminho_projeto(); ?>/mobile/produtos">
          <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/icon_produtos_geral.png"
            width="125"
            height="125"
            layout="responsive"
            alt="AMP">
          </amp-img>
        </a>
      </div>

      <div class="col-4 top20">
        <a href="<?php echo Util::caminho_projeto(); ?>/mobile/servicos">
          <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/icon_servicos_geral.png"
            width="125"
            height="125"
            layout="responsive"
            alt="AMP">
          </amp-img>
        </a>
      </div>


      <div class="col-4 top20">
        <a href="<?php echo Util::caminho_projeto(); ?>/mobile/orcamento">
          <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/icon_orcamento_geral.png"
            width="125"
            height="125"
            layout="responsive"
            alt="AMP">
          </amp-img>
        </a>
      </div>


      <div class="col-4 top20">
        <a href="<?php echo Util::caminho_projeto(); ?>/mobile/empresa">
          <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/icon_empresa_geral.png"
            width="125"
            height="125"
            layout="responsive"
            alt="AMP">
          </amp-img>
        </a>
      </div>

      <div class="col-4 top20">
        <a href="<?php echo Util::caminho_projeto(); ?>/mobile/contatos">
          <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/icon_contatos_geral.png"
            width="125"
            height="125"
            layout="responsive"
            alt="AMP">
          </amp-img>
        </a>
      </div>


      <div class="col-4 top20">
        <a href="<?php echo Util::caminho_projeto(); ?>/mobile/dicas">
          <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/icon_dicas_geral.png"
            width="125"
            height="125"
            layout="responsive"
            alt="AMP">
          </amp-img>
        </a>
      </div>


    </div>
    <!-- ======================================================================= -->
    <!--  PRODUTOS E SERVICOS -->
    <!-- ======================================================================= -->



<?php /*
    <div class="col-4 text-center">
      <a href="<?php echo Util::caminho_projeto() ?>/mobile/produtos">
        <div class="col-12 padding0 index-bg-icones">
          <amp-img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/icon_produtos_home.png" alt="A EMPRESA" height="60" width="60"></amp-img>
          <h1>
            <amp-fit-text
            width="480"
            height="100"
            layout="responsive"
            max-font-size="17">
            PRODUTOS
          </amp-fit-text>
        </h1>
      </div>
    </a>
  </div>

  <div class="col-4 text-center">
    <a href="<?php echo Util::caminho_projeto() ?>/mobile/servicos">
      <div class="index-bg-icones">
        <amp-img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/icon_servicos_home.png" alt="SERVIÇOS" height="60" width="60"></amp-img>
        <h1>
          <amp-fit-text
          width="480"
          height="100"
          layout="responsive"
          max-font-size="17">
          SERVIÇOS
        </amp-fit-text>
      </h1>
    </div>
  </a>
</div>

<div class="col-4 text-center">
  <a href="<?php echo Util::caminho_projeto() ?>/mobile/orcamento">
    <div class="index-bg-icones">
      <amp-img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/icon_orcamento_home.png" alt="ORÇAMENTO" height="60" width="60"></amp-img>
      <h1>
        <amp-fit-text
        width="480"
        height="100"
        layout="responsive"
        max-font-size="17">
        ORÇAMENTO
      </amp-fit-text>
    </h1>
  </div>
</a>
</div>


<div class="clearfix">  </div>

<div class="col-4 top10 text-center">
  <a href="<?php echo Util::caminho_projeto() ?>/mobile/empresa">
    <div class="index-bg-icones">
      <amp-img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/icon_empresa_home.png" alt="A EMPRESA" height="60" width="60"></amp-img>
      <h1>
        <amp-fit-text
        width="480"
        height="100"
        layout="responsive"
        max-font-size="17">
        A EMPRESA
      </amp-fit-text>
    </h1>
  </div>
</a>
</div>

<div class="col-4  top10 text-center">
  <a href="<?php echo Util::caminho_projeto() ?>/mobile/contatos">
    <div class="index-bg-icones">
      <amp-img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/icon_contatos_home.png" alt="CONTATOS" height="60" width="60"></amp-img>
      <h1>
        <amp-fit-text
        width="480"
        height="100"
        layout="responsive"
        max-font-size="17">
        CONTATOS
      </amp-fit-text>
    </h1>
  </div>
</a>
</div>

<div class="col-4  top10 text-center">
  <a href="<?php echo Util::caminho_projeto() ?>/mobile/dicas">
    <div class="index-bg-icones">
      <amp-img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/icon_dicas_home.png" alt="DICAS" height="60" width="60"></amp-img>
      <h1>
        <amp-fit-text
        width="450"
        height="100"
        layout="responsive"
        max-font-size="17">
        DICAS
      </amp-fit-text>
    </h1>
  </div>
</a>
</div>

*/ ?>

</div>


<?php require_once("./includes/rodape.php") ?>


</body>



</html>
