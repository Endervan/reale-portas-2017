
<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 9);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>
<!doctype html>
<html amp lang="pt-br">
<head>
  <?php require_once("../includes/head.php"); ?>
  <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>

  <style amp-custom>
  <?php require_once("../css/geral.css"); ?>
  <?php require_once("../css/topo_rodape.css"); ?>
  <?php require_once("../css/paginas.css");  //  ARQUIVO DA PAGINA ?>



  <?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 14); ?>
  .bg-interna{
    background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 67px center  no-repeat;
  }
  </style>

  <script async custom-element="amp-bind" src="https://cdn.ampproject.org/v0/amp-bind-0.1.js"></script>
  <script async custom-element="amp-fit-text" src="https://cdn.ampproject.org/v0/amp-fit-text-0.1.js"></script>




</head>

<body class="bg-interna">


  <?php
	require_once("../includes/topo.php") ?>

  <div class="row">

    <div class="col-12 text-center localizacao-pagina">
      <!-- ======================================================================= -->
      <!-- TITULO PAGINA  -->
      <!-- ======================================================================= -->
      <h6>DICAS</h6>
      <amp-img class="top20 " src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/barra_titulo.png" alt="" height="5" width="90"></amp-img>
      <!-- ======================================================================= -->
      <!-- TITULO PAGINA  -->
      <!-- ======================================================================= -->
    </div>
  </div>

  <!-- ======================================================================= -->
  <!--  CONTATOS -->
  <!-- ======================================================================= -->
  <div class="row bg_cinza_escuro relativo">

    <div class="col-6">
      <a href="tel:+55<?php Util::imprime($config[ddd1]); ?><?php Util::imprime($config[telefone1]); ?>">
        <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/telefone_dicas.png"
          width="214"
          height="50"
          layout="responsive"
          alt="AMP">
        </amp-img>
      </a>

      <div class="col-6 dicas">
        <h2>
          <amp-fit-text
          width="240"
          height="30"
          layout="responsive">
          <?php Util::imprime($config[ddd1]); ?><?php Util::imprime($config[telefone1]); ?>
          </amp-fit-text>
        </h2>
      </div>
    </div>


    <div class="col-6">
      <a href="<?php echo Util::caminho_projeto(); ?>/mobile/orcamento">
        <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/orcamento.png"
          width="224"
          height="50"
          layout="responsive"
          alt="AMP">
        </amp-img>
      </a>
    </div>



  </div>
  <!-- ======================================================================= -->
  <!--  CONTATOS -->
  <!-- ======================================================================= -->

  <!-- ======================================================================= -->
  <!--  DICAS -->
  <!-- ======================================================================= -->
  <div class="row dicas_geral">
    <?php
    $result = $obj_site->select("tb_dicas");
    if(mysql_num_rows($result) > 0){
      while($row = mysql_fetch_array($result)){
        $i=0;
        ?>

        <div class="col-6 top40">
          <div class="card">
            <a href="<?php echo Util::caminho_projeto(); ?>/mobile/dica/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
              <amp-img
              layout="responsive"
              height="200"
              width="280"
              src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>" alt="<?php Util::imprime($row[titulo]); ?>" >
            </amp-img>

            <div class="card-body ">
              <div class="desc_dicas top10">
                <h4 class="text-uppercase"><?php Util::imprime($row[titulo]); ?></h4>
              </div>

              <div class="w-25 ml-75 top5">
                  <amp-img
                  width="50"
                  height="50"
                  layout="responsive"
                  src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/circle.png" alt="">
                </amp-img>
                </a>
              </div>

            </div>
          </div>
        </div>

        <?php
        if ($i==1) {
          echo "<div class='clearfix'>  </div>";
        }else {
          $i++;
        }
      }
    }
    ?>
  </div>
  <!-- ======================================================================= -->
  <!--  DICAS -->
  <!-- ======================================================================= -->


  <!-- ======================================================================= -->
  <!--  PRODUTOS E SERVICOS -->
  <!-- ======================================================================= -->
  <div class="row bottom50">
    <div class="col-11 top40">

      <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/produtos_servicos.png"
        width="350"
        height="79"
        layout="responsive"
        alt="AMP">
      </amp-img>

    </div>

    <div class="clearfix">  </div>

    <div class="col-4 top20">
      <a href="<?php echo Util::caminho_projeto(); ?>/mobile/produtos">
        <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/icon_produtos_geral.png"
          width="125"
          height="125"
          layout="responsive"
          alt="AMP">
        </amp-img>
      </a>
    </div>

    <div class="col-4 top20">
      <a href="<?php echo Util::caminho_projeto(); ?>/mobile/servicos">
        <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/icon_servicos_geral.png"
          width="125"
          height="125"
          layout="responsive"
          alt="AMP">
        </amp-img>
      </a>
    </div>


    <div class="col-4 top20">
      <a href="<?php echo Util::caminho_projeto(); ?>/mobile/orcamento">
        <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/icon_orcamento_geral.png"
          width="125"
          height="125"
          layout="responsive"
          alt="AMP">
        </amp-img>
      </a>
    </div>



  </div>
  <!-- ======================================================================= -->
  <!--  PRODUTOS E SERVICOS -->
  <!-- ======================================================================= -->



  <?php require_once("../includes/rodape.php") ?>

</body>



</html>
