
<div class="container-fluid rodape">
	<div class="row">

		<a href="#" class="scrollup">scrollup</a>


		<div class="container">
			<div class="row bottom10">

				<!-- ======================================================================= -->
				<!-- LOGO    -->
				<!-- ======================================================================= -->
				<div class="col-3 top70">
					<a href="<?php echo Util::caminho_projeto() ?>/"  title="início">
						<img src="<?php echo Util::caminho_projeto() ?>/imgs/logo_rodape.png" alt="início" />
					</a>
				</div>
				<!-- ======================================================================= -->
				<!-- LOGO    -->
				<!-- ======================================================================= -->


				<div class="col-9 padding0 menu_rodape top75">
					<!-- ======================================================================= -->
					<!-- MENU    -->
					<!-- ======================================================================= -->
					<ul class="nav">
						<li class="nav-item">
							<a class="nav-link <?php if(Url::getURL( 0 ) == ""){ echo "active"; } ?>"
								href="<?php echo Util::caminho_projeto() ?>/">HOME
							</a>
						</li>

						<li class="nav-item">
							<a class="nav-link <?php if(Url::getURL( 0 ) == "empresa"){ echo "active"; } ?>"
								href="<?php echo Util::caminho_projeto() ?>/empresa">A EMPRESA
							</a>
						</li>

						<li class="nav-item">
							<a class="nav-link <?php if(Url::getURL( 0 ) == "produtos" or Url::getURL( 0 ) == "produto"){ echo "active"; } ?>"
								href="<?php echo Util::caminho_projeto() ?>/produtos">PRODUTOS
							</a>
						</li>

						<li class="nav-item">
							<a class="nav-link <?php if(Url::getURL( 0 ) == "servicos" or Url::getURL( 0 ) == "servico"){ echo "active"; } ?>"
								href="<?php echo Util::caminho_projeto() ?>/servicos">SERVIÇOS
							</a>
						</li>

						<li class="nav-item">
							<a class="nav-link <?php if(Url::getURL( 0 ) == "dicas" or Url::getURL( 0 ) == "dica"){ echo "active"; } ?>"
								href="<?php echo Util::caminho_projeto() ?>/dicas">DICAS
							</a>
						</li>

						<li class="nav-item">
							<a class="nav-link <?php if(Url::getURL( 0 ) == "contatos" or Url::getURL( 0 ) == "trabalhe-conosco"){ echo "active"; } ?>"
								href="<?php echo Util::caminho_projeto() ?>/contatos">CONTATOS
							</a>
						</li>


						<li class="nav-item">
							<a class="nav-link <?php if(Url::getURL( 0 ) == "orcamento"){ echo "active"; } ?>"
								href="<?php echo Util::caminho_projeto() ?>/orcamento">ORÇAMENTO
							</a>
						</li>

					</ul>
					<!-- ======================================================================= -->
					<!-- MENU    -->
					<!-- ======================================================================= -->
				</div>

				<!-- ======================================================================= -->
				<!-- ONDE ESTAMOS   -->
				<!-- ======================================================================= -->
				<div class="col-4  top65">
					<h2><span>ONDE ESTAMOS</span></h2>
					<div class="media top15">
						<img class="d-flex mr-3" src="<?php echo Util::caminho_projeto() ?>/imgs/icon_endereco_rodape.png" alt="">
						<div class="media-body">
							<p class="mt-0"><span><?php Util::imprime($config[endereco]); ?></span></p>
						</div>
					</div>

				</div>
				<!-- ======================================================================= -->
				<!-- ONDE ESTAMOS   -->
				<!-- ======================================================================= -->



				<div class="col-6 top65 endereco_rodape">
					<h2><span>FALE CONOSCO</span></h2>
					<!-- ======================================================================= -->
					<!-- telefones  -->
					<!-- ======================================================================= -->
					<div class="row top30">
						<div class="col-6 media">
							<i class="d-flex  mr-3 fa fa-phone fa-2x" aria-hidden="true"></i>
							<div class="media-body align-self-center">
								<h2 class="mt-0 mr-3"><?php Util::imprime($config[ddd1]); ?> <?php Util::imprime($config[telefone1]); ?></h2>
							</div>
						</div>
						<?php if (!empty($config[telefone2])): ?>
							<div class="col-6 media">
								<i class="d-flex  mr-3 fa fa-whatsapp fa-2x" aria-hidden="true"></i>
								<div class="media-body align-self-center">
									<h2 class="mt-0 mr-3"><?php Util::imprime($config[ddd2]); ?> <?php Util::imprime($config[telefone2]); ?></h2>
								</div>
							</div>
						<?php endif; ?>

						<?php if (!empty($config[telefone3])): ?>
							<div class="col-6 media">
								<i class="d-flex  mr-3 fa fa-phone fa-2x" aria-hidden="true"></i>
								<div class="media-body align-self-center">
									<h2 class="mt-0 mr-3"><?php Util::imprime($config[ddd3]); ?> <?php Util::imprime($config[telefone3]); ?></h2>
								</div>
							</div>
						<?php endif; ?>

						<?php if (!empty($config[telefone4])): ?>
							<div class="col-6 media">
								<i class="d-flex  mr-3 fa fa-phone fa-2x" aria-hidden="true"></i>
								<div class="media-body align-self-center">
									<h2 class="mt-0 mr-3"><?php Util::imprime($config[ddd4]); ?> <?php Util::imprime($config[telefone4]); ?></h2>
								</div>
							</div>
						<?php endif; ?>
					</div>

					<!-- ======================================================================= -->
					<!-- telefones  -->
					<!-- ======================================================================= -->
				</div>



				<!-- ======================================================================= -->
				<!-- redes sociais    -->
				<!-- ======================================================================= -->
				<div class="col-2 redes top85">
					<div class="row">
						<div class="col text-right padding0 top30">
							<?php if ($config[google_plus] != "") { ?>
								<a href="<?php Util::imprime($config[google_plus]); ?>" title="Google Plus" target="_blank" >
									<i class="fa fa-google-plus-official pt20 fa-2x right15"></i>
								</a>
							<?php } ?>
						</div>
						<?php 	/*
						<?php if ($config[facebook] != "") { ?>
						<a href="<?php Util::imprime($config[facebook]); ?>" title="facebook" target="_blank" >
						<i class="fa fa-facebook-official fa-2x top20 right15"></i>
						</a>
						<?php } ?>

						<?php if ($config[instagram] != "") { ?>
						<a href="<?php Util::imprime($config[instagram]); ?>" title="instagram" target="_blank" >
						<i class="fa fa-instagram fa-2x top20 right15"></i>
						</a>
						<?php } ?>

						*/ ?>
						<div class="col">
							<a href="http://www.homewebbrasil.com.br" target="_blank">
								<img class="top20" src="<?php echo Util::caminho_projeto() ?>/imgs/logo_homeweb.png"  alt="">
							</a>
						</div>

					</div>
				</div>
				<!-- ======================================================================= -->
				<!-- redes sociais    -->
				<!-- ======================================================================= -->




			</div>
		</div>
	</div>
</div>

<div class="container-fluid">
	<div class="row rodape-preto lato">
		<div class="col-12 text-center top10 bottom10">
			<h5>© Copyright  REALE PORTAS DE ACM</h5>
		</div>
	</div>
</div>
